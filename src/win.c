/*
 * win.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Windows interface for steganography
 *
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include "resource.h"
#include "stegano.h"

/********************* Win GDI Interface ************************/
int cap = 0, size = 0, plen = 0;
uint8_t *buf = NULL, pass[256];

/**
 * Read in image file
 */
int ReadImage(HWND hwndDlg)
{
    HANDLE f;
    DWORD r, t;
    wchar_t szFilePathName[MAX_PATH];

    size = cap = plen = 0;
    memset(pass, 0, sizeof(pass));
    GetDlgItemTextW(hwndDlg, IDC_MAINDLG_PASS, szFilePathName, sizeof(pass));
    if(szFilePathName[0]) {
        for(r = 0; szFilePathName[r]; r++);
        plen = (int)WideCharToMultiByte(CP_UTF8, 0, szFilePathName, r, pass, sizeof(pass), NULL, NULL);
        pass[plen] = 0;
    }
    GetDlgItemTextW(hwndDlg, IDC_MAINDLG_SOURCE, szFilePathName, sizeof(szFilePathName) / sizeof(szFilePathName[0]));
    if(!szFilePathName[0]) { MessageBoxA(hwndDlg, "No input image.", "Steganography", MB_ICONERROR); return 0; }
    if(buf) free(buf);
    buf = (uint8_t*)malloc(MAXIMGSIZE*1024*1024);
    if(!buf) { MessageBoxA(hwndDlg, "Unable to allocate memory.", "Steganography", MB_ICONERROR); return 0; }
    f = CreateFileW(szFilePathName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if(f){
        r = GetFileSize(f, NULL);
        if(r > MAXIMGSIZE*1024*1024) r = MAXIMGSIZE*1024*1024;
        size = (int)r;
        memset(buf, 0, MAXIMGSIZE*1024*1024);
        if(!ReadFile(f, buf, r, &t, NULL) || r != t) size = 0;
        CloseHandle(f);
    }
    if(size > 0) cap = getcapacity(buf, size);
    if(cap < 1) { size = cap = 0; MessageBoxA(hwndDlg, "Unable to read image.", "Steganography", MB_ICONERROR); }
    wsprintf(szFilePathName, L"Capacity %d bytes.", cap);
    SetDlgItemText(hwndDlg, IDC_MAINDLG_STATUS, szFilePathName);
    return cap > 0;
}

/**
 * File select button clicked
 */
INT_PTR MainDlgSelectClick(HWND hwndDlg) {
    OPENFILENAME ofn;
    TCHAR lpstrFile[MAX_PATH];

    ZeroMemory(&ofn, sizeof ofn);
    ZeroMemory(lpstrFile, sizeof lpstrFile);
    ofn.lStructSize = sizeof ofn;
    ofn.hwndOwner = hwndDlg;
    ofn.lpstrFilter = TEXT("Images (webp, png, gif, jpeg)\0*.webp;*.png;*.gif;*.jpg;*.jpeg\0\0");
    ofn.lpstrFile = lpstrFile;
    ofn.nMaxFile = MAX_PATH;
    ofn.Flags = OFN_FILEMUSTEXIST;
    if (GetOpenFileName(&ofn)) {
        SetDlgItemText(hwndDlg, IDC_MAINDLG_SOURCE, ofn.lpstrFile);
        ReadImage(hwndDlg);
        if(buf) { free(buf); buf = NULL; size = 0; }
    }

    return TRUE;
}

/**
 * Load button clicked
 */
INT_PTR MainDlgLoadClick(HWND hwndDlg) {
    wchar_t *lpMsg;
    uint8_t *message;
    int len, i;

    SetDlgItemTextW(hwndDlg, IDC_MAINDLG_MSG, L"");
    if(!ReadImage(hwndDlg)) return FALSE;
    message = (uint8_t*)malloc(cap);
    if(!message) { free(buf); buf = NULL; MessageBoxA(hwndDlg, "Unable to allocate memory.", "Steganography", MB_ICONERROR); return FALSE; }
    len = decode(buf, size, pass, plen, message, cap);
    if(len > 0) {
        lpMsg = (wchar_t*)malloc(2 * (len + 1) * sizeof(wchar_t));
        if(!lpMsg) { free(message); free(buf); buf = NULL; MessageBoxA(hwndDlg, "Unable to allocate memory.", "Steganography", MB_ICONERROR); return FALSE; }
        memset(lpMsg, 0, 2 * (len + 1) * sizeof(wchar_t));
        len = MultiByteToWideChar(CP_UTF8, 0, message, -1, lpMsg, len);
        lpMsg[len] = 0;
        /* fix CRLFs */
        for(i = 0; lpMsg[i] && i < len; i++)
            if(lpMsg[i] == L'\n' && (!i || lpMsg[i - 1] != L'\r')) {
                memmove(&lpMsg[i + 1], &lpMsg[i], (len - i + 1) * sizeof(wchar_t));
                len++; lpMsg[i++] = L'\r';
            }
        SetDlgItemTextW(hwndDlg, IDC_MAINDLG_MSG, lpMsg);
        free(lpMsg);
    } else {
        MessageBoxA(hwndDlg, "Probably no message in image or bad password.", "Steganography", MB_ICONERROR);
    }
    free(message);
    free(buf); buf = NULL;
    SetDlgItemText(hwndDlg, IDC_MAINDLG_PASS, L"");
    return TRUE;
}

/**
 * Save button clicked
 */
INT_PTR MainDlgSaveClick(HWND hwndDlg) {
    HANDLE f;
    DWORD r = 0;
    OPENFILENAME ofn;
    wchar_t lpstrFile[MAX_PATH];
    wchar_t *lpMsg;
    uint8_t *message;
    int len;

    if(!ReadImage(hwndDlg)) return FALSE;
    lpMsg = (wchar_t*)malloc((cap + 1) * sizeof(wchar_t));
    if(!lpMsg) { free(buf); buf = NULL; MessageBoxA(hwndDlg, "Unable to allocate memory.", "Steganography", MB_ICONERROR); return FALSE; }
    memset(lpMsg, 0, (cap + 1) * sizeof(wchar_t));
    GetDlgItemTextW(hwndDlg, IDC_MAINDLG_MSG, lpMsg, cap);
    if(!lpMsg[0]) { free(lpMsg); free(buf); buf = NULL; MessageBoxA(hwndDlg, "No message.", "Steganography", MB_ICONERROR); return FALSE; }
    for(len = 0; lpMsg[len]; len++);
    message = (uint8_t*)malloc((len + 1) * 4 + 32);
    if(!message) { free(lpMsg); free(buf); buf = NULL; MessageBoxA(hwndDlg, "Unable to allocate memory.", "Steganography", MB_ICONERROR); return FALSE; }
    memset(message, 0, (len + 1) * 4 + 32);
    len = (int)WideCharToMultiByte(CP_UTF8, 0, lpMsg, len, message, len * 4, NULL, NULL);
    free(lpMsg);
    size = encode(buf, size, pass, plen, message, len);
    if(size > 0) {
        ZeroMemory(&ofn, sizeof ofn);
        ZeroMemory(lpstrFile, sizeof lpstrFile);
        memcpy(lpstrFile, L"stegano.webp", 26);
        ofn.lStructSize = sizeof ofn;
        ofn.hwndOwner = hwndDlg;
        ofn.lpstrFilter = TEXT("webp\0*.webp\0\0");
        ofn.lpstrFile = lpstrFile;
        ofn.nMaxFile = MAX_PATH;
        ofn.Flags = OFN_OVERWRITEPROMPT;
        if (GetSaveFileName(&ofn)) {
            f = CreateFileW(ofn.lpstrFile, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, 0, NULL);
            if(f){
                if(!WriteFile(f, buf, size, &r, NULL) || r != size) r = 0;
                CloseHandle(f);
            }
            if(!r) {
                MessageBoxA(hwndDlg, "Unable to write image.", "Steganography", MB_ICONERROR);
            }
        }
    } else {
        MessageBoxA(hwndDlg, "Unable to generate output image.", "Steganography", MB_ICONERROR);
    }
    free(message);
    free(buf); buf = NULL;
    SetDlgItemText(hwndDlg, IDC_MAINDLG_PASS, L"");
    return TRUE;
}

/**
 * Event dispatcher
 */
static INT_PTR CALLBACK MainDlgProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    UNREFERENCED_PARAMETER(lParam);

    switch (uMsg) {
        case WM_INITDIALOG: SetDlgItemText(hwndDlg, IDC_MAINDLG_STATUS, L"Capacity 0 bytes."); return TRUE;
        case WM_CLOSE: EndDialog(hwndDlg, 0); return TRUE;
        case WM_COMMAND:
            switch (LOWORD(wParam)) {
                case IDC_MAINDLG_SELECT:return (HIWORD(wParam) == BN_CLICKED) ? MainDlgSelectClick(hwndDlg) : FALSE;
                case IDC_MAINDLG_LOAD:  return (HIWORD(wParam) == BN_CLICKED) ? MainDlgLoadClick(hwndDlg) : FALSE;
                case IDC_MAINDLG_SAVE:  return (HIWORD(wParam) == BN_CLICKED) ? MainDlgSaveClick(hwndDlg) : FALSE;
                default:                return FALSE;
            }
        default: return FALSE;
    }
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszArgument, int nCmdShow) {
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpszArgument);
    UNREFERENCED_PARAMETER(nCmdShow);

    return DialogBoxParam(hInstance, MAKEINTRESOURCE(IDC_MAINDLG), NULL, MainDlgProc, (LPARAM) hInstance);
}
