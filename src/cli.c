/*
 * cli.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Command line interface for steganography
 *
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "stegano.h"

/****************** Command Line Interface *********************/

void usage(char *cmd)
{
    printf("Steganography by bztsrc@gitlab GPLv3+\r\n\r\n");
    printf("%s [-p <passwd>] <image> [message]\r\n\r\n", cmd);
    printf("  -p <passwd>   set encryption password\r\n");
    printf("  image         image file (WEBP, PNG, GIF, JPEG)\r\n");
    printf("  message       if given, encodes message and saves image\r\n");
    printf("                otherwise decodes message from image to stdout\r\n\r\n");
    exit(1);
}

int main(int argc, char **argv)
{
    FILE *f;
    char *pass = NULL, *img = NULL, *msg = NULL, *out, *c;
    uint8_t *buf = NULL;
    int i, size = 0, plen, len = 0;

    /* parse command line */
    for(i = 1; i < argc && argv[i]; i++)
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case 'p': pass = argv[++i]; break;
                default: usage(argv[0]); break;
            }
        } else
        if(!img) img = argv[i]; else
        if(!msg) msg = argv[i]; else
            usage(argv[0]);
    if(!img) usage(argv[0]);
    plen = pass ? strlen(pass) : 0;

    /* get the image data */
    f = fopen(img, "rb");
    if(f) {
        fseek(f, 0L, SEEK_END);
        size = (int)ftell(f);
        fseek(f, 0L, SEEK_SET);
        buf = (uint8_t*)malloc(MAXIMGSIZE*1024*1024);
        if(!buf) { fprintf(stderr, "stegano: unable to allocate memory\r\n"); exit(1); }
        memset(buf, 0, MAXIMGSIZE*1024*1024);
        i = fread(buf, 1, size, f);
        fclose(f);
        len = getcapacity(buf, size);
    }
    if(!buf || size < 1 || len < 1) {
        fprintf(stderr, "stegano: unable to read '%s'\r\n", img);
        exit(1);
    }

    /* do the thing */
    if(!msg || !*msg) {
        /* no message given, so decode */
        fprintf(stderr, "stegano: capacity %d bytes.\r\n", len);
        msg = (char*)malloc(len + 1);
        if(!msg) { fprintf(stderr, "stegano: unable to allocate memory\r\n"); exit(1); }
        memset(msg, 0, len + 1);
        len = decode(buf, size, (uint8_t*)pass, plen, (uint8_t*)msg, len);
        if(len > 0)
            printf("%s\r\n", msg);
        else {
            fprintf(stderr, "stegano: probably no message in image or bad password.\r\n");
            exit(1);
        }
        free(msg);
    } else {
        /* with a message specified, encode it into the image */
        i = strlen(msg);
        out = c = (char*)malloc(i + 32);
        if(!out) { fprintf(stderr, "stegano: unable to allocate memory\r\n"); exit(1); }
        memset(out, 0, i + 32);
        for(; *msg; msg++)
            if(*msg == '\\') {
                msg++;
                switch(*msg) {
                    case '\\': *c++ = '\\'; break;
                    case 't': *c++ = '\t'; break;
                    case 'r': *c++ = '\r'; break;
                    case 'n': *c++ = '\n'; break;
                    default: *c++ = '\\'; *c++ = *msg; break;
                }
            } else
                *c++ = *msg;
        i = (int)((uintptr_t)c - (uintptr_t)out);
        msg = out;
        if(i >= len) { fprintf(stderr, "stegano: message longer than image capacity.\r\n"); exit(1); }
        out = (char*)malloc(strlen(img) + 8);
        if(!out) { fprintf(stderr, "stegano: unable to allocate memory\r\n"); exit(1); }
        strcpy(out, img); c = strrchr(out, '.'); if(!c) { c = out + strlen(out); } strcpy(c, ".webp");
        size = encode(buf, size, (uint8_t*)pass, plen, (uint8_t*)msg, i);
        if(size > 0) {
            f = fopen(out, "wb");
            if(f) {
                fwrite(buf, 1, size, f);
                fclose(f);
                fprintf(stderr, "stegano: '%s' saved.\r\n", out);
            } else {
                fprintf(stderr, "stegano: unable to write '%s'.\r\n", out);
                exit(1);
            }
        } else {
            fprintf(stderr, "stegano: unable to generate output image.\r\n");
            exit(1);
        }
        free(out);
        free(msg);
    }
    free(buf);
    return 0;
}
