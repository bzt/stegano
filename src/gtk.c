/*
 * gtk.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief GTK interface for steganography
 *
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include "stegano.h"

/********************* GTK Interface ************************/
GtkWidget *window, *vbox, *hbox1, *img, *sel, *passw, *hbox2, *load, *save, *msg, *status;
GtkTextBuffer *msgbuf;
int cap = 0, size = 0, plen = 0;
uint8_t *buf = NULL, *pass = NULL;

/**
 * Gtk dialog
 */
void messagebox(char *msg)
{
    GtkWidget *mbox = gtk_message_dialog_new(GTK_WINDOW(window), 0, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "%s", msg);
    gtk_window_set_title(GTK_WINDOW(mbox), "Steganography");
    gtk_dialog_run(GTK_DIALOG(mbox));
    gtk_widget_destroy(mbox);
}

/**
 * Read in image file
 */
int read_image()
{
    char *fn = (char*)gtk_entry_get_text(GTK_ENTRY(img)), tmp[64];
    FILE *f;

    size = cap = plen = 0;
    pass = (char*)gtk_entry_get_text(GTK_ENTRY(passw));
    if(pass) plen = strlen(pass);

    if(!fn || !*fn) { messagebox("No input image."); return 0; }
    if(buf) free(buf);
    buf = (uint8_t*)malloc(MAXIMGSIZE*1024*1024);
    if(!buf) { messagebox("Unable to allocate memory."); return 0; }
    f = fopen(fn, "rb");
    if(f){
        fseek(f, 0L, SEEK_END);
        size = (int)ftell(f);
        if(size > MAXIMGSIZE*1024*1024) size = MAXIMGSIZE*1024*1024;
        fseek(f, 0L, SEEK_SET);
        memset(buf, 0, MAXIMGSIZE*1024*1024);
        if(fread(buf, 1, size, f) != size) size = 0;
        fclose(f);
    }
    if(size > 0) cap = getcapacity(buf, size);
    if(cap < 1) { size = cap = 0; messagebox("Unable to read image."); }
    sprintf(tmp, "Capacity %d bytes.", cap);
    gtk_label_set_label(GTK_LABEL(status), tmp);
    return cap > 0;
}

/**
 * File select button clicked
 */
static void on_select_clicked(GtkButton *btn, gpointer data)
{
    char *fn;
    GtkWidget *chooser = gtk_file_chooser_dialog_new("Open", NULL, GTK_FILE_CHOOSER_ACTION_OPEN, "_Cancel",
        GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, NULL);
    if(gtk_dialog_run(GTK_DIALOG(chooser)) == GTK_RESPONSE_ACCEPT) {
        fn = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(chooser));
        if(fn) {
            gtk_entry_set_text(GTK_ENTRY(img), fn);
            read_image();
            if(buf) { free(buf); buf = NULL; size = 0; }
            free(fn);
        }
    }
    gtk_widget_destroy(chooser);
}

/**
 * Load button clicked
 */
static void on_load_clicked(GtkButton *btn, gpointer data)
{
    uint8_t *message;
    int len;

    gtk_text_buffer_set_text(msgbuf, "", -1);
    if(!read_image()) return;
    message = (uint8_t*)malloc(cap);
    if(!message) { free(buf); buf = NULL; messagebox("Unable to allocate memory."); return; }
    len = decode(buf, size, pass, plen, message, cap);
    if(len > 0) {
        gtk_text_buffer_set_text(msgbuf, message, -1);
    } else {
        messagebox("Probably no message in image or bad password.");
    }
    free(message);
    free(buf); buf = NULL;
    gtk_entry_buffer_set_text(gtk_entry_get_buffer(GTK_ENTRY(passw)), "", -1);
}

/**
 * Save button clicked
 */
static void on_save_clicked(GtkButton *btn, gpointer data)
{
    FILE *f;
    GtkWidget *chooser;
    GtkTextIter start, end;
    char *msgtxt, *fn;
    uint8_t *message;
    int len;

    if(!read_image()) return;
    gtk_text_buffer_get_start_iter(msgbuf, &start);
    gtk_text_buffer_get_end_iter(msgbuf, &end);
    msgtxt = gtk_text_buffer_get_text(msgbuf, &start, &end, FALSE);
    if(!msgtxt || !*msgtxt) { free(buf); buf = NULL; messagebox("No message."); return; }
    len = strlen(msgtxt);
    message = (uint8_t*)malloc(len + 32);
    if(!message) { free(msgtxt); free(buf); buf = NULL; messagebox("Unable to allocate memory."); return; }
    memset(message, 0, len + 32);
    strcpy(message, msgtxt);
    free(msgtxt);
    size = encode(buf, size, pass, plen, message, len);
    if(size > 0) {
        chooser = gtk_file_chooser_dialog_new("Save As", NULL, GTK_FILE_CHOOSER_ACTION_SAVE, "_Cancel",
            GTK_RESPONSE_CANCEL, "_Save", GTK_RESPONSE_ACCEPT, NULL);
        gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(chooser), TRUE);
        gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(chooser), "stegano.webp");
        if(gtk_dialog_run(GTK_DIALOG(chooser)) == GTK_RESPONSE_ACCEPT) {
            fn = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(chooser));
            if(fn) {
                f = fopen(fn, "wb");
                if(f) {
                    if(fwrite(buf, 1, size, f) != size) size = 0;
                    fclose(f);
                } else
                    size = 0;
                if(!size)
                    messagebox("Unable to write image.");
                free(fn);
            }
        }
        gtk_widget_destroy(chooser);
    } else {
        messagebox("Unable to generate output image.");
    }
    free(message);
    free(buf); buf = NULL;
    gtk_entry_buffer_set_text(gtk_entry_get_buffer(GTK_ENTRY(passw)), "", -1);
}

/**
 * Exit handler
 */
static void destroy(GtkWidget *widget, gpointer data)
{
    gtk_main_quit();
}

/**
 * Main application entry point
 */
int main(int argc, char **argv)
{
    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Steganography 0.0.1");
    gtk_window_set_default_size(GTK_WINDOW(window), 420, 240);
    g_signal_connect(window, "destroy", G_CALLBACK(destroy), NULL);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_container_add(GTK_CONTAINER(window), vbox);

    hbox1 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start(GTK_BOX(vbox), hbox1, FALSE, TRUE, 0);

    img = gtk_entry_new();
    gtk_entry_set_placeholder_text(GTK_ENTRY(img), "Image");
    gtk_box_pack_start(GTK_BOX(hbox1), img, TRUE, TRUE, 0);

    sel = gtk_button_new_with_label("...");
    g_signal_connect(sel, "clicked", G_CALLBACK(on_select_clicked), NULL);
    gtk_box_pack_start(GTK_BOX(hbox1), sel, FALSE, TRUE, 0);

    passw = gtk_entry_new();
    gtk_entry_set_placeholder_text(GTK_ENTRY(passw), "Password");
    gtk_entry_set_visibility(GTK_ENTRY(passw), FALSE);
    gtk_box_pack_start(GTK_BOX(vbox), passw, FALSE, TRUE, 0);

    hbox2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    gtk_box_pack_start(GTK_BOX(vbox), hbox2, FALSE, TRUE, 0);

    load = gtk_button_new_with_label("Load");
    g_signal_connect(load, "clicked", G_CALLBACK(on_load_clicked), NULL);
    gtk_box_pack_start(GTK_BOX(hbox2), load, TRUE, TRUE, 5);

    save = gtk_button_new_with_label("Save");
    g_signal_connect(save, "clicked", G_CALLBACK(on_save_clicked), NULL);
    gtk_box_pack_start(GTK_BOX(hbox2), save, TRUE, TRUE, 5);

    msgbuf = gtk_text_buffer_new(NULL);
    msg = gtk_text_view_new_with_buffer(msgbuf);
    gtk_box_pack_start(GTK_BOX(vbox), msg, TRUE, TRUE, 0);

    status = gtk_label_new("Capacity 0 bytes.");
    gtk_label_set_xalign(GTK_LABEL(status), 0);
    gtk_box_pack_start(GTK_BOX(vbox), status, FALSE, FALSE, 2);

    gtk_widget_show_all(GTK_WIDGET(window));
    gtk_main();
    return 0;
}
