/*
 * stegano.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Public API for steganography
 *
 */

#ifndef STEGANO_H
#define STEGANO_H

#define MAXIMGSIZE  128 /* Megabytes, largest compressed image we can generate */

int getcapacity(uint8_t *buf, int size);
int decode(uint8_t *buf, int size, uint8_t *pass, int plen, uint8_t *message, int mlen);
int encode(uint8_t *buf, int size, uint8_t *pass, int plen, uint8_t *message, int mlen);

#endif
